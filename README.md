# WorldCupScoreBoard_ACagriB
Three classes are implemented for application

	At first, 
	* Tests.java class is developed for testing usage stories of the application
	
	Then,
	* Match.java class represents football games which is a entity class
	* WorldCupScoreBoard.java class represents a scoreboard and basically its operations which covers requirements logic
	
	Lastly,
	* UsageOfBoardWithMainMethod.java class is developed for seeing a scenario on console, also this class is used for runnable jar creation.

## Explanation of main class

1.Matches added in this order:
----------------------------------
1-"Mexico", "Canada"

2-"Spain", "Brazil"

3-"Germany", "France"

4-"Uruguay", "Italy"

5-"Argentina", "Australia"


2.Matches are listed:
---------------------------------
You can review the output below the doc, matches are listed in filo/lifo logic while all scores are zero. (Mexico and Canada match is firstly inserted but lastly written to the console)


3.Scores are updated
---------------------------------
1-Argentina, Australia, 3, 1

2-Uruguay, Italy, 6, 6

3-Germany, France, 2, 2

4-Spain, Brazil, 10, 2

5-Mexico, Canada, 0, 5


4.Uruguay - Italy match is ended by finish match method
---------------------------------
You can review the output below the doc.


5.Matches are listed again
---------------------------------
You can review the output below the doc.


6.New match added to the score board
---------------------------------
Japan-Portugal : 9-3, this match is added to the score board by start match method.


7.Matches are listed again
---------------------------------
Please notice that lastly inserted match is presented firstly at the score board although it has same total goal number as the secondly listed match. You can review the output below the doc.


## Please notice "WorldCupScoreBoard_ACagriB.jar" inside referenced libraries folder

In any java projects, this solution can be used as library


## The jar "DirectRunnable_WorldCupScoreBoard_ACagriB.jar" can be directly run by double click

 * Please navigate to the your bin folder under java home 
 * For example : "C:\xxx\Java64\jdk1.8.0_161\bin"
 * Open command prompt 
 * Copy the jar to the same location
 * Write down "java -jar DirectRunnable_WorldCupScoreBoard_ACagriB.jar" and press enter
 * Jar will automatically give the expected output related to the given (Live Data - Coding Excercise.pdf) requirement list
 * Comamnd Line : "C:\xxxxxxxxxxxxx\Java64\jdk1.8.0_161\bin>java -jar DirectRunnable_WorldCupScoreBoard_ACagriB.jar"


## Output of the execution

Score Board
----------------------------------------
Argentina-Australia : 0-0

Uruguay-Italy : 0-0

Germany-France : 0-0

Spain-Brazil : 0-0

Mexico-Canada : 0-0


Score Board
----------------------------------------
Uruguay-Italy : 6-6

Spain-Brazil : 10-2

Mexico-Canada : 0-5

Argentina-Australia : 3-1

Germany-France : 2-2


Uruguay-Italy : 6-6 is ended.


Score Board
----------------------------------------
Spain-Brazil : 10-2

Mexico-Canada : 0-5

Argentina-Australia : 3-1

Germany-France : 2-2


New match added.


Japan-Portugal : 9-3

Spain-Brazil : 10-2

Mexico-Canada : 0-5

Argentina-Australia : 3-1

Germany-France : 2-2

