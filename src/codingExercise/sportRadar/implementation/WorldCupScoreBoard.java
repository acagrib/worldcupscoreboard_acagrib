package codingExercise.sportRadar.implementation;

import java.util.ArrayList;
import java.util.List;

public class WorldCupScoreBoard {

	private List<Match> matchList = new ArrayList<>();

	public List<Match> getMatches() {
		return matchList;
	}

	public void startMatch(String home, String away) throws Exception {
		if (home.equals("") || away.equals("")) {
			throw new Exception("Team names should be entered!");
		} else {
			matchList.add(0, new Match(home, away));
		}
	}

	public void updateMatchScore(String home, String away, int homeScore, int awayScore) throws Exception {
		Match match = new Match(home, away);
		if (homeScore < 0 || awayScore < 0) {
			throw new Exception("Scores can not be negative!");
		} else if (!matchList.contains(match)) {
			throw new Exception("This match can not be found : " + match + " ");
		} else {
			int i = matchList.indexOf(match);
			Match match_l = matchList.get(i);
			match_l.setHomeScore(homeScore);
			match_l.setAwayScore(awayScore);
		}
	}

	public void finishMatch(String home, String away) throws Exception {
		Match match = new Match(home, away);
		if (matchList.contains(match)) {
			int i = matchList.indexOf(match);
			String m = matchList.get(i).toString();
			matchList.remove(match);
			System.out.println("\n" + m + " is ended.");
		} else {
			throw new Exception("This match can not be found : " + match + " ");
		}
	}

	public void showAllMatches() {
		if (matchList == null || matchList.isEmpty()) {
			System.out.println("No match has been started.");
		} else {
			matchList.stream().sorted().forEach(System.out::println);
		}
	}
}