package codingExercise.sportRadar.implementation;

public class Match implements Comparable<Object> {

	private String home;
	private String away;
	private int homeScore;
	private int awayScore;

	public Match(String home, String away) {
		this.home = home;
		this.away = away;
	}

	public Match(String home, String away, int homeScore, int awayScore) {
		this.home = home;
		this.away = away;
		this.homeScore = homeScore;
		this.awayScore = awayScore;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public String getAway() {
		return away;
	}

	public void setAway(String away) {
		this.away = away;
	}

	public int getHomeScore() {
		return homeScore;
	}

	public void setHomeScore(int homeScore) {
		this.homeScore = homeScore;
	}

	public int getAwayScore() {
		return awayScore;
	}

	public void setAwayScore(int awayScore) {
		this.awayScore = awayScore;
	}

	public Integer getTotalGoals() {
		return Integer.valueOf(homeScore + awayScore);
	}

	public int compareTo(Object o) {
		Match other = (Match) o;
		return other.getTotalGoals().compareTo(getTotalGoals());
	}

	@Override
	public boolean equals(Object o) {
		Match other = (Match) o;
		return home.equals(other.home) && away.equals(other.away);
	}

	@Override
	public String toString() {
		return home + "-" + away + " : " + homeScore + "-" + awayScore;
	}
}