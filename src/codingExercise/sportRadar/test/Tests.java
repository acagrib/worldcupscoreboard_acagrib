package codingExercise.sportRadar.test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import codingExercise.sportRadar.implementation.Match;
import codingExercise.sportRadar.implementation.WorldCupScoreBoard;

class Tests {

	@Test
	public void createMatchTest() {
		Match match = new Match("Mexico", "Canada");
		assertTrue(match.getTotalGoals() == 0);
	}

	@Test
	public void createMatchWithScoreTest() {
		Match match = new Match("Germany", "France", 4, 1);
		assertTrue(match.getTotalGoals() == 5);
	}

	@Test
	public void startMatchTest() throws Exception {
		WorldCupScoreBoard wcsb = new WorldCupScoreBoard();
		String home = "home";
		String away = "away";
		wcsb.startMatch(home, away);
		assertTrue(wcsb.getMatches().size() == 1);
	}

	@Test
	public void startMatchFailTest() {
		try {
			WorldCupScoreBoard wcsb = new WorldCupScoreBoard();
			String home = "home";
			String away = "";
			wcsb.startMatch(home, away);
			fail("Need to have an exception.");
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	public void finishMatchTest() throws Exception {
		WorldCupScoreBoard wcsb = new WorldCupScoreBoard();
		String home = "home";
		String away = "away";
		wcsb.startMatch(home, away);
		assertTrue(wcsb.getMatches().size() == 1);
		wcsb.finishMatch(home, away);
		assertTrue(wcsb.getMatches().size() == 0);
	}

	@Test
	public void finishMatchFailTest() throws Exception {
		try {
			WorldCupScoreBoard wcsb = new WorldCupScoreBoard();
			String home = "home";
			String away = "away";
			wcsb.finishMatch(home, away);
			fail("Need to have an exception.");
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	public void updateMatchScoreTest() throws Exception {
		WorldCupScoreBoard wcsb = new WorldCupScoreBoard();
		String home = "home";
		String away = "away";
		wcsb.startMatch(home, away);
		assertTrue(wcsb.getMatches().size() == 1);
		wcsb.updateMatchScore(home, away, 2, 3);
		assertTrue(wcsb.getMatches().get(wcsb.getMatches().indexOf(new Match(home, away))).getTotalGoals() == 5);
	}

	@Test
	public void updateMatchScoreWithNegativeFailTest() throws Exception {
		try {
			WorldCupScoreBoard wcsb = new WorldCupScoreBoard();
			String home = "home";
			String away = "away";
			wcsb.startMatch(home, away);
			assertTrue(wcsb.getMatches().size() == 1);
			wcsb.updateMatchScore(home, away, -1, 9);
			fail("Need to have an exception.");
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	public void updateMatchScoreFailTest() throws Exception {
		try {
			WorldCupScoreBoard wcsb = new WorldCupScoreBoard();
			String home = "home";
			String away = "away";
			wcsb.updateMatchScore(home, away, 3, 1);
			fail("Need to have an exception.");
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	public void getMatchesTest() throws Exception {
		WorldCupScoreBoard wcsb = new WorldCupScoreBoard();
		String home = "home";
		String away = "away";
		wcsb.startMatch(home, away);
		home = "home2";
		away = "away2";
		wcsb.startMatch(home, away);
		assertTrue(wcsb.getMatches().size() == 2);
	}

	@Test
	public void getMatchesBeforeNoMathCreatedTest() {
		WorldCupScoreBoard wcsb = new WorldCupScoreBoard();
		wcsb.getMatches();
		assertTrue(wcsb.getMatches().size() == 0);
	}
}