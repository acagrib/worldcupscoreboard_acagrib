package codingExercise.sportRadar.usageWithMain;

import codingExercise.sportRadar.implementation.WorldCupScoreBoard;

public class UsageOfBoardWithMainMethod {

	public static void main(String[] args) throws Exception {
		WorldCupScoreBoard wcsb = new WorldCupScoreBoard();

		wcsb.startMatch("Mexico", "Canada");
		wcsb.startMatch("Spain", "Brazil");
		wcsb.startMatch("Germany", "France");
		wcsb.startMatch("Uruguay", "Italy");
		wcsb.startMatch("Argentina", "Australia");
		System.out.println("Score Board");
		System.out.println("----------------------------------------");
		wcsb.showAllMatches();

		wcsb.updateMatchScore("Argentina", "Australia", 3, 1);
		wcsb.updateMatchScore("Uruguay", "Italy", 6, 6);
		wcsb.updateMatchScore("Germany", "France", 2, 2);
		wcsb.updateMatchScore("Spain", "Brazil", 10, 2);
		wcsb.updateMatchScore("Mexico", "Canada", 0, 5);
		System.out.println("\nScore Board");
		System.out.println("----------------------------------------");
		wcsb.showAllMatches();

		wcsb.finishMatch("Uruguay", "Italy");

		System.out.println("\nScore Board");
		System.out.println("----------------------------------------");
		wcsb.showAllMatches();

		wcsb.startMatch("Japan", "Portugal");
		wcsb.updateMatchScore("Japan", "Portugal", 9, 3);
		System.out.println("\nNew match added.\n");
		wcsb.showAllMatches();
	}
}